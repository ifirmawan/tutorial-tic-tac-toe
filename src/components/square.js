import React from 'react'

export const Square = (props) => {
  const { value } = props
  return (
    // eslint-disable-next-line react/react-in-jsx-scope
    <button className='square' onClick={() => props.onClick()}>
      {value}
    </button>
  )
}
